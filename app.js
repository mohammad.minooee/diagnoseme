var nools = require("nools");

var Message = function(message) {
	this.text = message;
};

var symptoms = ['headache', 'neck', 'lightsensitivity', 'dizziness', 'cough', 'asthma', 'brownSputum', 'yellowSputum',
	'muscleAches', 'rhinorrhea', 'soreThroat', 'bringingUpBlood', 'backPain', 'frequentUrination', 'urinaryPain',
	'darkUrine', 'traveled', 'vaginalDischarge', 'heatExhaustion', 'frequencyOfFever'
];
var questions = [
	'آیا سردرد شدید دارید؟',
	'آیا موقع خم کردن سر به جلو درد گردن دارید؟',
	'آیا از نور زیاد اذیت میشوید؟',
	'آیا دچار خواب آلودگی یا منگی هستید؟',
	'آیا سرفه میکنید؟',
	'آیا موقع استراحت هم تنگی نفس دارید؟',
	'آیا همراه با سرفه خلط قهوه ای رنگ دارید؟',
	'آیا همراه با سرفه خلط زرد مایل به خاکستری دارید؟',
	'آیا سردرد خفیف دارید؟',
	'آیا درد عضلات دارید؟',
	'آیا آبریزش بینی دارید؟',
	'آیا گلودرد دارید؟',
	'آیا موقع سرفه خون بالا می آورید؟',
	'آیا درد پشت و کمر در یک سمت یا هردو سمت دارید؟',
	'آیا تکرر ادرار دارید؟',
	'آیا در هنگام ادرار درد دارید؟',
	'آیا ادرار کدر دارید؟',
	'آیا اخیرا از سفر خارج بازگشتید؟',
	'آیا مونث هستید؟ و ترشح غیر طبیعی از مهبل با درد یا بدون درد دارید؟',
	'آیا چندین ساعت در نور شدید آفتاب و هوای گرم بوده اید؟',
	'آیا در چند هفته گدشته تب های مکرری داشته اید که همراه کاهش وزن باشد؟'
];

// for (var i = 0; i < symptoms.length; i++) {
// console.log(i + ' ' + symptoms[i])
// };

var flow = nools.compile(__dirname + "/diagnosis.nools");
// var	Message = flow.getDefined("message");
var Patient = flow.getDefined("patient");

var session = flow.getSession()
	.on("diagnosis", function(diagnosis) {
		d = diagnosis;
		console.log(d);
	});

var p = new Patient({
	name: "mohamamd",
	symptoms: {headache: true}
})
// for (var i = 0; i < symptoms.length; i++) {
// 	p.symptoms[symptoms[i]] = false;
// };
console.log(p);
session.assert(p);

session.match();

// session.assert(new Message("yes"));

// session.match();