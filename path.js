var keyboards = require('./keyboards');
 

var path = {};



var s= ['highHeadache', 'neck', 'lightsensitivity', 'dizziness', 'cough', 'asthma', 'brownSputum', 'yellowSputum',
		'lowHeadache','muscleAches', 'rhinorrhea', 'soreThroat', 'bringingUpBlood', 'backPain', 'frequentUrination', 'urinaryPain',
		'darkUrine', 'traveled', 'vaginalDischarge', 'heatExhaustion', 'frequencyOfFever'
	],
	q = [
		'آیا سردرد شدید دارید؟',
		'آیا موقع خم کردن سر به جلو درد گردن دارید؟',
		'آیا از نور زیاد اذیت میشوید؟',
		'آیا دچار خواب آلودگی یا منگی هستید؟',
		'آیا سرفه میکنید؟',
		'آیا موقع استراحت هم تنگی نفس دارید؟',
		'آیا همراه با سرفه خلط قهوه ای رنگ دارید؟',
		'آیا همراه با سرفه خلط زرد مایل به خاکستری دارید؟',
		'آیا سردرد خفیف دارید؟',
		'آیا درد عضلات دارید؟',
		'آیا آبریزش بینی دارید؟',
		'آیا گلودرد دارید؟',
		'آیا موقع سرفه خون بالا می آورید؟',
		'آیا درد پشت و کمر در یک سمت یا هردو سمت دارید؟',
		'آیا تکرر ادرار دارید؟',
		'آیا در هنگام ادرار درد دارید؟',
		'آیا ادرار کدر دارید؟',
		'آیا اخیرا از سفر خارج بازگشتید؟',
		'آیا مونث هستید؟ و ترشح غیر طبیعی از مهبل با درد یا بدون درد دارید؟',
		'آیا چندین ساعت در نور شدید آفتاب و هوای گرم بوده اید؟',
		'آیا در چند هفته گدشته تب های مکرری داشته اید که همراه کاهش وزن باشد؟'
	];

path['mainMenu'] = {
	text: '',
	goTo: {
		'معاینه جدید': 'q0',
		'2': 'help'
	}
};

path['q0'] = {
	symptom: s[0],
	text: q[0],
	goTo: {
		'خیر': 'q1',
		'بله': 'A'
	}
};
path['q1'] = {
	symptom: s[1],
	text: q[1],
	goTo: {
		'خیر': 'q2',
		'بله': 'A'
	}
};

path['q2'] = {
	symptom: s[2],
	text: q[2],
	goTo: {
		'خیر': 'q3',
		'بله': 'A'
	}
};

path['q3'] = {
	symptom: s[3],
	text: q[3],
	goTo: {
		'خیر': 'q4',
		'بله': 'A'
	}
};

path['q4'] = {
	symptom: s[4],
	text: q[4],
	goTo: {
		'خیر': 'q11',
		'بله': 'q5'
	}
};

path['q5'] = {
	symptom: s[5],
	text: q[5],
	goTo: {
		'خیر': 'q6',
		'بله': 'B'
	}
};

path['q6'] = {
	symptom: s[6],
	text: q[6],
	goTo: {
		'خیر': 'q7',
		'بله': 'B'
	}
};

path['q7'] = {
	symptom: s[7],
	text: q[7],
	goTo: {
		'خیر': 'q8',
		'بله': 'D'
	}
};

path['q8'] = {
	symptom: s[8],
	text: q[8],
	goTo: {
		'خیر': 'q9',
		'بله': 'E'
	}
};

path['q9'] = {
	symptom: s[9],
	text: q[9],
	goTo: {
		'خیر': 'q10',
		'بله': 'E'
	}
};

path['q10'] = {
	symptom: s[10],
	text: q[10],
	goTo: {
		'خیر': 'q12',
		'بله': 'E'
	}
};

path['q11'] = {
	symptom: s[11],
	text: q[11],
	goTo: {
		'خیر': 'q13',
		'بله': 'C'
	}
};

path['q12'] = {
	symptom: s[12],
	text: q[12],
	goTo: {
		'خیر': 'G',
		'بله': 'F'
	}
};

path['q13'] = {
	symptom: s[13],
	text: q[13],
	goTo: {
		'خیر': 'q14',
		'بله': 'H'
	}
};

path['q14'] = {
	symptom: s[14],
	text: q[14],
	goTo: {
		'خیر': 'q15',
		'بله': 'H'
	}
};

path['q15'] = {
	symptom: s[15],
	text: q[15],
	goTo: {
		'خیر': 'q16',
		'بله': 'H'
	}
};

path['q16'] = {
	symptom: s[16],
	text: q[16],
	goTo: {
		'خیر': 'q17',
		'بله': 'H'
	}
};

path['q17'] = {
	symptom: s[17],
	text: q[17],
	goTo: {
		'خیر': 'q18',
		'بله': 'I'
	}
};

path['q18'] = {
	symptom: s[18],
	text: q[18],
	goTo: {
		'خیر': 'q19',
		'بله': 'J'
	}
};

path['q20'] = {
	symptom: s[20],
	text: q[20],
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};




path['A'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['B'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['C'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['D'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['E'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['F'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['G'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['H'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['I'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['J'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['K'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['L'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};path['M'] = {
	symptom:'',
	text:'نتیجه:',
	goTo: {
		'خیر': 'M',
		'بله': 'L'
	}
};
module.exports = path;