var TelegramBot = require('telegram-bot-api');
var keyboards = require('./keyboards');
var symptoms = keyboards.symptoms;
var diagnoses = keyboards.diagnose;

var yes = keyboards.confirm,
    no = keyboards.cancel;
var ans = {
    'بله': true,
    'خیر': false
}
var Sessions = require('./Sessions');
var path = require('./path');
var nools = require("nools");
var util = require('util');


var bot = new TelegramBot({
    token: '198256575:AAG5-JcjCAHk2NMdEEOnm2SAGpLGdEgi3vU',
    updates: {
        enabled: true
    }
});


var flow = nools.compile(__dirname + "/diagnosis.nools");
// var  Message = flow.getDefined("message");
var Patient = flow.getDefined("patient");

var session = flow.getSession()
    .on("diagnosis", function(diagnosis) {
        d = diagnosis;
        console.log(d);
        bot.sendMessage({
            chat_id: d.name,
            text: diagnoses[d.diagnosis],
            reply_markup: JSON.stringify(keyboards.start)
        }, function(final_err, final_msg) {
            console.log(final_err);
        });
        session.retract(diagnosis);
        diagnosis={};
        session.assert(diagnosis);
    });



console.log('bot server started...');


// Any kind of message 
bot.on('message', function(msg) {

    var chatId = msg.chat.id;
    // console.log(chatId);
    console.log(chatId + ' ' + msg.text);



    if (msg.text === "/start") {
        // console.log(chatId + ' ' + msg.text);
        var mySession = Sessions.newSession(msg.chat.id);
        mySession.state = 'mainMenu';
        mySession.step = 0;
        mySession.id = msg.chat.id;
        var Patient = flow.getDefined("Patient");
        var Diagnosis = flow.getDefined("Diagnosis");
        var d = new Diagnosis({
            name: null,
            diagnosis: null
        });
        mySession.patient = new Patient({
            name: msg.chat.id,
            symptoms: {}
        });
        session.assert(d);

        console.log(util.inspect(mySession, false, null))

        bot.sendMessage({
            chat_id: msg.chat.id,
            text: 'انتخاب کنید',
            reply_markup: JSON.stringify(keyboards.main_menu)
        }, function(final_err, final_msg) {
            console.log(final_err);
        });
    } else {

        var Diagnosis = flow.getDefined("Diagnosis");
        console.log('dddddd'+util.inspect(Diagnosis, false, null))

        var text;

        var mySession = Sessions.getSession(msg.chat.id);
        console.log(util.inspect(mySession, false, null))
        if (typeof mySession === 'undefined') {
            text = '/start';
            console.log('1');
        } else {
            console.log('2');
            text = path[path[mySession.state].goTo[msg.text]].text;
            console.log('3')
            var s = path[mySession.state].symptom;
            if (typeof text != 'undefined' && typeof s != 'undefined') {

                mySession.patient.symptoms[s] = ans[msg.text];
                console.log('4');
                session.assert(mySession.patient);
                console.log('5');
                session.match();
                console.log('6');
            }
            mySession.state = path[mySession.state].goTo[msg.text];
            console.log('7');
        }
        var key=keyboards.sure;
        if(text =='نتیجه:')
            key=keyboards.start;

        bot.sendMessage({
            chat_id: msg.chat.id,
            text: text,
            reply_markup: JSON.stringify(key)
        }, function(final_err, final_msg) {
            console.log(final_err);
        });
    }


});