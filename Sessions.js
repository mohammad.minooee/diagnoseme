  
 
var Sessions = {};

module.exports = {

 
	getSession: function ( id ) {
		return Sessions[id];
	},

	setSessionProperty: function ( Session, key, value ) {
 		Sessions[Session.id][key] = value;
 	},

	newSession: function ( id ) {
		Sessions[id] = {};
		return Sessions[id];
	},

	deleteSession: function ( id ) {
		delete Sessions[id];
	}
};