var nools = require("nools");

var Message = function(message) {
	this.message = message;
};

var flow = nools.flow("Hello World", function(flow) {
	this.rule("Goodbye", [Message, "m", "m.message =~ /.*goodbye$/"], function(facts) {
		this.emit('hello');
	});
});

var session = flow.getSession()
	.on("hello", function() {
		console.log('good');
	});;
session.assert(new Message("goodbye"));
session.match();